<?php
//title+description?q=Guide

$books = new SmartXML(file_get_contents("../data/books.xml"));

//validating Scope
$scope = isset(Router::$uri[1]) ? Router::$uri[1] : 0;
$scopeDirtyArray = explode("+",$scope);
$validFilters = array("author", "title", "genre", "price", "publish_date", "description");
$scopeArray = !empty($scope) ? array_intersect($scopeDirtyArray, $validFilters) : $validFilters;

//extracting text query
$query = isset($_GET['q']) ? $_GET['q']: null;


// XPATH query construction
$xquery = "/catalog/book[";
$results = null;

if (!empty($query) && !empty($scope)) {  //when scope is specified
    foreach ($scopeArray as $sc) {
    	$xquery .= $sc."[contains(., '".$query."')] or ";
    }
    $xquery = preg_replace("/ or $/","]",$xquery);
    $results = $books->xpath->query($xquery);
}

if ($results != null && $results->count()) {
	$results = Paging::page($results);
    echo "<results>";
    foreach ($results as $book) {
        echo $book . "\n";
    }
    echo "</results>";
} else {
    echo IO::error("no matching results");
}