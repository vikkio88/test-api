<?php

/**************************************************************************************************
 ** Paging class **
 *
 * Static class with method useful to paging results
 *
 **************************************************************************************************/
class Paging
{
	public static function page($results)
	{
		$pageNum = isset($_GET['p']) ? $_GET['p'] : -1;
		$forPage = isset($_GET['forP']) ? $_GET['forP'] : -1;

		if($pageNum!=-1 AND $forPage != -1 AND $results->count()){ //if result is not null and paging filters are set
			//load an Array cause Result is not an array
			$resultArray = array();
			foreach ($results as $book) {
				array_push($resultArray,$book);
			}
			//chunking result array
			$pagedResults = array_chunk($resultArray,$forPage);
			if($pageNum<=count($pagedResults))
				return $pagedResults[$pageNum-1];
			else
				return $pagedResults[count($pagedResults)-1];
		}

		return $results;
	}
}