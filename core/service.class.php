<?php

class Service
{

    public $file;
    public $api;

    public static $variableStack = array();

    public function __construct($file)
    {
        $this->file = $file . ".php";

        $this->logged = 0;
    }

    public function renderPage()
    {
        if(isset($_GET['output']) && $_GET['output'] == "json"){
            //instead of changing classes and their output methods I will intercept
            //and parse Echoing
            ob_start();
            $this->run("services/" . $this->file);
            $str = ob_get_contents();
            ob_end_clean();
            $xml = simplexml_load_string($str);
            $json = json_encode($xml);

            //printing results
            header('Content-type: application/json; charset=utf-8');
            echo $json;

        } else {
            header('Content-type: application/xml; charset=utf-8');
            echo "<?xml version=\"1.0\"?>\n";
            return $this->run("services/" . $this->file);
        }
    }

    public function run($file, $_variables = null)
    {
        self::$variableStack[] = $_variables;

        if (is_array($_variables)) {
            foreach ($_variables as $name => $value) {
                ${$name} = $value;
            }
        }

        if (file_exists(PATH_BASE . $file)) {
            include(PATH_BASE . $file);

            return true;
        }

        array_pop(self::$variableStack);

        return false;
    }

}